export default function intToRomanNumerals(num) {
    const romanNumeralMap = { 
        1000: "M",
        900: "CM",
        500: "D",
        400: "CD",
        100: "C",
        90: "XC",
        50: "L",
        40: "XL",
        10: "X",
        9: "IX",
        5: "V",
        4: "IV",
        1: "I"
    };
    const numbers = Object.keys(romanNumeralMap).reverse();

    if (!(Number.isInteger(num))) { 
        throw new TypeError("The value you entered is not an integer.");
    }
    else if (num >= 4000) { 
        throw new Error("The number you entered is too large to convert to Roman numerals. Please enter a number between 1 and 3999.");
    }
    else if (num < 0) { 
        throw new Error("The number you entered is a negative number. Please enter a number between 1 and 3999.");
    }
    else {
        if (num == 0) { 
            return "";
        }

        while (num > 0) { 
            for (let value = 0; value < numbers.length; value++) { 
                if (num >= numbers[value]) {
                    let numeral = romanNumeralMap[numbers[value]];
                    return numeral + intToRomanNumerals(num - numbers[value]);
                }
            }
        }
    }
}

// source code: https://gitlab.com/chibueze_anakor/roman-numeral-converter/-/blob/main/IntToRomanNumerals.js